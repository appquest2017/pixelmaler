package ch.appquest.pixelmaler.model;

import android.graphics.Color;
import android.graphics.Paint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;

public class PixelContainer extends Observable {
    public static final int GRID_SIZE = 13;

    private Pixel[][] pixels = new Pixel[GRID_SIZE][GRID_SIZE];
    private static PixelContainer instance = null;

    private Paint default_paint = new Paint();

    private PixelContainer() {
        default_paint.setColor(Color.WHITE);
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                pixels[x][y] = new Pixel(default_paint);
            }
        }
        printPixels();
    }

    public void setPixel(int x, int y, Paint color) {
        try {
            pixels[x][y] = new Pixel(color);
        } catch (ArrayIndexOutOfBoundsException ex) {
            setChanged();
            notifyObservers("Cannot write Pixel to Point(" + x + "," + y + ")");
        }
    }

    public Pixel getPixel(int x, int y) {
        return pixels[x][y];
    }

    public static PixelContainer getInstance() {
        if (instance == null) {
            instance = new PixelContainer();
        }
        return instance;
    }

    public JSONArray getJSONOutput() {
        JSONArray points = new JSONArray();

        for (int y = 0; y < GRID_SIZE; y++) {
            for (int x = 0; x < GRID_SIZE; x++) {
                JSONObject curr = new JSONObject();
                try {
                    curr.put("y", y);
                    curr.put("x", x);
                    curr.put("color", pixels[x][y].getARGB());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                points.put(curr);
            }
        }
        return points;
    }

    public void printPixels() {
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                System.out.print(pixels[x][y].getARGB() + " ");
            }
            System.out.println();
        }
    }

    public void reset() {
        default_paint.setColor(Color.WHITE);
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                pixels[x][y] = new Pixel(default_paint);
            }
        }
    }
}
