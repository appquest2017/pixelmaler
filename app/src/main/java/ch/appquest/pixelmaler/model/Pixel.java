package ch.appquest.pixelmaler.model;

import android.graphics.Paint;

public class Pixel {
    private Paint paint;

    public Pixel(Paint color) {
        color.setStyle(Paint.Style.FILL_AND_STROKE);
        paint = color;
    }

    public String getARGB() {
        return String.format("#%06X", (0xFFFFFF & paint.getColor())) + "FF";
    }

    public Paint getPaint() {
        return paint;
    }
}
