package ch.appquest.pixelmaler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ch.appquest.pixelmaler.lib.SaveAndLoad;
import ch.appquest.pixelmaler.model.Pixel;
import ch.appquest.pixelmaler.model.PixelContainer;

/**
 * Die DrawingView ist für die Darstellung und Verwaltung der Zeichenfläche
 * zuständig.
 */
public class DrawingView extends View {
    private int maxX = 0, maxY = 0;

    private Path drawPath = new Path();
    private Paint drawPaint = new Paint();
    private Paint linePaint = new Paint();
    private boolean isErasing = false;
    private PixelContainer pixelContainer;
    private Context ctx;

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(20);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        linePaint.setColor(0xFF666666);
        linePaint.setAntiAlias(true);
        linePaint.setStrokeWidth(1.0f);
        linePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (maxX == 0) {
            maxX = canvas.getWidth();
            maxY = canvas.getHeight();
        }

        final int stepSizeX = (int) Math.ceil((double) maxX / PixelContainer.GRID_SIZE);
        final int stepSizeY = (int) Math.ceil((double) maxY / PixelContainer.GRID_SIZE);

        pixelContainer = PixelContainer.getInstance();

        //Restore data
        for (int x = 0; x < PixelContainer.GRID_SIZE; x++) {
            for (int y = 0; y < PixelContainer.GRID_SIZE; y++) {
                Pixel curr = pixelContainer.getPixel(x, y);
                Rect rect = new Rect(x * stepSizeX, y * stepSizeY, x * stepSizeX + stepSizeX, y * stepSizeY + stepSizeY);
                canvas.drawRect(rect, curr.getPaint());
            }
        }

        for (int i = 0; i <= maxX; i += stepSizeX) {
            canvas.drawLine(i, 0, i, maxY, linePaint);
        }

        for (int i = 0; i <= maxY; i += stepSizeY) {
            canvas.drawLine(0, i, maxX, i, linePaint);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                int x = (int) Math.floor(touchX / maxX * PixelContainer.GRID_SIZE);
                int y = (int) Math.floor(touchY / maxY * PixelContainer.GRID_SIZE);

                Paint temp = new Paint();
                temp.setColor(drawPaint.getColor());

                pixelContainer.setPixel(x, y, temp);

                break;
            case MotionEvent.ACTION_MOVE:

                x = (int) Math.floor(touchX / maxX * PixelContainer.GRID_SIZE);
                y = (int) Math.floor(touchY / maxY * PixelContainer.GRID_SIZE);

                temp = new Paint();
                temp.setColor(drawPaint.getColor());

                pixelContainer.setPixel(x, y, temp);

                break;
            case MotionEvent.ACTION_UP:
                save();
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    public void save() {
        SaveAndLoad.saveData("data.tmp", pixelContainer.getJSONOutput().toString(), ctx);
    }

    public void load() {
        String data = SaveAndLoad.loadData("data.tmp", ctx);
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject curr = jsonArray.getJSONObject(i);

                String hex = curr.getString("color").substring(0, 7);
                int color = Color.parseColor(hex);

                Paint paint = new Paint();
                paint.setColor(color);
                PixelContainer.getInstance().setPixel(curr.getInt("x"), curr.getInt("y"), paint);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        invalidate();
    }

    public void startNew() {
        pixelContainer.reset();
        invalidate();
    }

    public void setErase(boolean isErase) {
        isErasing = isErase;
        if (isErasing) {
            drawPaint.setColor(Color.WHITE);
        }
    }

    public boolean isErasing() {
        return isErasing;
    }

    public void setColor(String color) {
        invalidate();
        drawPaint.setColor(Color.parseColor(color));
    }
}
